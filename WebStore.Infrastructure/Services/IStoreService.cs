﻿using System.Collections.Generic;
using System.Threading.Tasks;
using WebStore.Domain.Model;

namespace WebStore.Infrastructure.Services
{
    public interface IStoreService
    {
        ResultWrap<Customer> CreateCustomer(string firstName, string lastName, string phoneNumber);
        ResultWrap<List<Customer>> GetAllCustomers();
        Task<Customer> GetCustomer(long idCustomer);
        ResultWrap<Product> CreateProduct(string name, string description, decimal price);
        ResultWrap<List<Product>> GetAllProducts();
        Task<Product> GetProduct(long idProduct);
        ResultWrap<Basket> ShowBasket(long idCustomer);
        Task<ResultWrap<BasketProducts>> AddProductInBasket(long idCustomer, long idProduct, int amount);
        ResultWrap<BasketProducts> RemoveProductInBasket(long idCustomer, long idProduct);
        ResultWrap<List<Purchase>> ShowCustomerPurhases(long idCustomer);
        ResultWrap<List<Purchase>> BuyProductInBasket(long idCustomer);


    }
}