﻿using Microsoft.Data.SqlClient;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Threading.Tasks;
using WebStore.Domain.Model;

namespace WebStore.Infrastructure.Services
{
    public class AdoStoreService : IStoreService
    {
        SqlConnection _connection;
        public AdoStoreService(IConfiguration configuration)
        {
            _connection = new SqlConnection(configuration.GetConnectionString("DefaultConnection"));
        }

        public async Task<Customer> GetCustomer(long idCustomer)
        {
            SqlDataReader reader = null;
            try
            {
                _connection.Open();
                string sql = @$"SELECT TOP (1) * FROM Customers WHERE Id = {idCustomer}";
                var command = new SqlCommand(sql, _connection);
                reader = command.ExecuteReader();

                Customer customer = null;
                if (reader?.HasRows ?? false)
                {
                    while (reader.Read())
                    {
                        customer = new Customer
                        {
                            Id = reader.GetInt64(0),
                            FirstName = reader.GetString(1),
                            LastName = reader.GetString(2),
                            PhoneNumber = reader.GetString(3)
                        };
                    }

                }
                return customer;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                reader?.Close();
                _connection.Close();
            }
        }

        public ResultWrap<Customer> CreateCustomer(string firstName, string lastName, string phoneNumber)
        {
            SqlDataReader reader = null;
            try
            {
                _connection.Open();
                string sql = @$"INSERT INTO Customers(FirstName,LastName,PhoneNumber) VALUES ('{firstName}','{lastName}','{phoneNumber}')";
                var command = new SqlCommand(sql, _connection);
                command.ExecuteNonQuery();

                sql = @$"SELECT TOP (1) * FROM Customers cust  WHERE cust.FirstName = '{firstName}' AND cust.LastName = '{lastName}' AND cust.PhoneNumber = '{phoneNumber}'";
                command = new SqlCommand(sql, _connection);
                reader = command.ExecuteReader();

                Customer customer = null;
                if (reader?.HasRows ?? false)
                {
                    while (reader.Read())
                    {
                        customer = new Customer
                        {
                            Id = reader.GetInt64(0),
                            FirstName = reader.GetString(1),
                            LastName = reader.GetString(2),
                            PhoneNumber = reader.GetString(3)
                        };
                    }
                }

                return new ResultWrap<Customer>(ResultValues.Success, customer);
            }
            catch (Exception)
            {
                return new ResultWrap<Customer>(ResultValues.Error, null);
            }
            finally
            {
                reader?.Close();
                _connection.Close();
            }
        }

        public ResultWrap<List<Customer>> GetAllCustomers()
        {
            SqlDataReader reader = null;
            try
            {
                _connection.Open();
                string sql = @$"SELECT * FROM Customers";
                var command = new SqlCommand(sql, _connection);
                reader = command.ExecuteReader();

                List<Customer> customers = new List<Customer>();
                if (reader?.HasRows ?? false)
                {
                    while (reader.Read())
                    {
                        customers.Add(new Customer()
                        {
                            Id = reader.GetInt64(0),
                            FirstName = reader.GetString(1),
                            LastName = reader.GetString(2),
                            PhoneNumber = reader.GetString(3)
                        });
                    }
                }
                else
                {
                    return new ResultWrap<List<Customer>>(ResultValues.NotFound, null);
                }

                return new ResultWrap<List<Customer>>(ResultValues.Success, customers);
            }
            catch (Exception)
            {
                return new ResultWrap<List<Customer>>(ResultValues.Error, null);
            }
            finally
            {
                reader?.Close();
                _connection.Close();
            }
        }

        public ResultWrap<Product> CreateProduct(string name, string description, decimal price)
        {
            SqlDataReader reader = null;
            try
            {
                _connection.Open();
                string sql = @$"INSERT INTO Products(Name, Description, Price) VALUES('{name}','{description}', {price})";
                var command = new SqlCommand(sql, _connection);
                command.ExecuteNonQuery();

                sql = @$"SELECT TOP (1) * FROM Products pr WHERE pr.Name = '{name}' AND pr.Description = '{description}' AND pr.Price = {price}";
                command = new SqlCommand(sql, _connection);
                reader = command.ExecuteReader();

                Product product = null;
                if (reader?.HasRows ?? false)
                {
                    while (reader.Read())
                    {
                        product = new Product
                        {
                            Id = reader.GetInt64(0),
                            Name = reader.GetString(1),
                            Description = reader.GetString(2),
                            Price = reader.GetDecimal(3)
                        };
                    }
                }

                return new ResultWrap<Product>(ResultValues.Success, product);
            }
            catch (Exception)
            {
                return new ResultWrap<Product>(ResultValues.Error, null);
            }
            finally
            {
                reader?.Close();
                _connection.Close();
            }
        }

        public async Task<Product> GetProduct(long idProduct)
        {
            SqlDataReader reader = null;
            try
            {
                _connection.Open();
                string sql = @$"SELECT TOP (1) * FROM Products pr WHERE pr.Id = {idProduct}";
                var command = new SqlCommand(sql, _connection);
                reader = command.ExecuteReader();

                Product product = null;
                if (reader?.HasRows ?? false)
                {
                    while (reader.Read())
                    {
                        product = new Product
                        {
                            Id = reader.GetInt64(0),
                            Name = reader.GetString(1),
                            Description = !reader.IsDBNull(2) ? reader.GetString(2) : null,
                            Price = reader.GetDecimal(3)
                        };
                    }
                }

                return product;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                reader?.Close();
                _connection.Close();
            }
        }

        public ResultWrap<List<Product>> GetAllProducts()
        {
            SqlDataReader reader = null;
            try
            {
                _connection.Open();
                string sql = @$"SELECT * FROM Products";
                var command = new SqlCommand(sql, _connection);
                reader = command.ExecuteReader();

                List<Product> products = new List<Product>();
                if (reader?.HasRows ?? false)
                {
                    while (reader.Read())
                    {
                        products.Add(new Product()
                        {
                            Id = reader.GetInt64(0),
                            Name = reader.GetString(1),
                            Description = !reader.IsDBNull(2) ? reader.GetString(2) : null,
                            Price = reader.GetDecimal(3)
                        });
                    }
                }
                else
                {
                    return new ResultWrap<List<Product>>(ResultValues.NotFound, products);
                }

                return new ResultWrap<List<Product>>(ResultValues.Success, products);
            }
            catch (Exception)
            {
                return new ResultWrap<List<Product>>(ResultValues.Error, null);
            }
            finally
            {
                reader?.Close();
                _connection.Close();
            }
        }

        public ResultWrap<Basket> ShowBasket(long idCustomer)
        {
            SqlDataReader reader = null;
            try
            {
                _connection.Open();
                string sql = @$"SELECT TOP (1) * FROM Basket WHERE CustomerId = {idCustomer}";
                var command = new SqlCommand(sql, _connection);
                reader = command.ExecuteReader();

                Basket basket = null;
                if (reader?.HasRows ?? false)
                {
                    while (reader.Read())
                    {
                        basket = new Basket
                        {
                            Id = reader.GetInt64(0),
                            CustomerId = reader.GetInt64(1)
                        };
                    }
                }
                else
                {
                    return new ResultWrap<Basket>(ResultValues.NotFound, null);
                }

                return new ResultWrap<Basket>(ResultValues.Success, basket);
            }
            catch (Exception)
            {
                return new ResultWrap<Basket>(ResultValues.Error, null);
            }
            finally
            {
                reader?.Close();
                _connection.Close();
            }
        }

        public ResultWrap<List<Purchase>> ShowCustomerPurhases(long idCustomer)
        {
            SqlDataReader reader = null;
            try
            {
                _connection.Open();
                string sql = @$"SELECT * FROM Purchases WHERE CustomerId = {idCustomer}";
                var command = new SqlCommand(sql, _connection);
                reader = command.ExecuteReader();

                List<Purchase> purchases = new List<Purchase>();
                if (reader?.HasRows ?? false)
                {
                    while (reader.Read())
                    {
                        purchases.Add(new Purchase()
                        {
                            Id = reader.GetInt64(0),
                            CustomerId = reader.GetInt64(1),
                            ProductId = reader.GetInt64(2),
                            Amount = reader.GetInt32(3)
                        });
                    }
                }
                else
                {
                    return new ResultWrap<List<Purchase>>(ResultValues.NotFound, null);
                }

                return new ResultWrap<List<Purchase>>(ResultValues.Success, purchases);
            }
            catch (Exception)
            {
                return new ResultWrap<List<Purchase>>(ResultValues.Error, null);
            }
            finally
            {
                reader?.Close();
                _connection.Close();
            }
        }

        public async Task<ResultWrap<BasketProducts>> AddProductInBasket(long idCustomer, long idProduct, int amount)
        {
            Basket basket = GetBasket(idCustomer);
            if (basket == null)
            {
                return new ResultWrap<BasketProducts>(ResultValues.NotFound, null);
            }

            SqlDataReader reader = null;
            try
            {
                _connection.Open();
                string sql = @$"INSERT INTO BasketProducts(BasketId, ProductId, Amount) VALUES ({basket.Id},{idProduct},{amount})";
                var command = new SqlCommand(sql, _connection);
                command.ExecuteNonQuery();

                sql = @$"SELECT TOP (1) * FROM BasketProducts WHERE BasketId = {basket.Id} AND ProductId = {idProduct} AND Amount = {amount} ORDER BY Id DESC";
                command = new SqlCommand(sql, _connection);
                reader = command.ExecuteReader();

                BasketProducts bp = null;
                if (reader?.HasRows ?? false)
                {
                    while (reader.Read())
                    {
                        bp = new BasketProducts
                        {
                            Id = reader.GetInt64(0),
                            BasketId = reader.GetInt64(1),
                            ProductId = reader.GetInt64(2),
                            Amount = reader.GetInt32(3)

                        };
                    }
                }
                else
                {
                    return new ResultWrap<BasketProducts>(ResultValues.NotFound, null);
                }

                return new ResultWrap<BasketProducts>(ResultValues.Success, bp);
            }
            catch (Exception)
            {
                return new ResultWrap<BasketProducts>(ResultValues.Error, null);
            }
            finally
            {
                reader?.Close();
                _connection.Close();
            }
        }

        public ResultWrap<BasketProducts> RemoveProductInBasket(long idCustomer, long idProduct)
        {
            Basket basket = GetBasket(idCustomer);
            if (basket == null)
            {
                return new ResultWrap<BasketProducts>(ResultValues.NotFound, null);
            }

            SqlDataReader reader = null;
            try
            {
                _connection.Open();
                string sql = @$" SELECT TOP (1) * FROM BasketProducts WHERE BasketId = {basket.Id} AND ProductId = {idProduct}";
                var command = new SqlCommand(sql, _connection);
                reader = command.ExecuteReader();

                BasketProducts bp = new BasketProducts();
                if (reader?.HasRows ?? false)
                {
                    while (reader.Read())
                    {
                        bp = new BasketProducts
                        {
                            Id = reader.GetInt64(0),
                            BasketId = reader.GetInt64(1),
                            ProductId = reader.GetInt64(2),
                            Amount = reader.GetInt32(3)
                        };
                    }
                }
                else
                {
                    return new ResultWrap<BasketProducts>(ResultValues.NotFound, null);
                }
                reader?.Close();

                sql = @$"DELETE FROM BasketProducts WHERE BasketId = {basket.Id} AND ProductId = {idProduct}";
                command = new SqlCommand(sql, _connection);
                command.ExecuteNonQuery();

                return new ResultWrap<BasketProducts>(ResultValues.Success, bp);
            }
            catch (Exception ex)
            {
                return new ResultWrap<BasketProducts>(ResultValues.Error, null);
            }
            finally
            {
                reader?.Close();
                _connection.Close();
            }
        }

        public ResultWrap<List<Purchase>> BuyProductInBasket(long idCustomer)
        {
            Basket basket = GetBasket(idCustomer);
            if (basket == null)
            {
                return new ResultWrap<List<Purchase>>(ResultValues.NotFound, null);
            }

            try
            {
                _connection.Open();
                string sql = @$"SELECT * FROM BasketProducts WHERE BasketId = {basket.Id}";
                var command = new SqlCommand(sql, _connection);
                List<BasketProducts> bpList = new List<BasketProducts>();
                using (var reader1 = command.ExecuteReader())
                {
                    if (reader1?.HasRows ?? false)
                    {
                        while (reader1.Read())
                        {
                            bpList.Add(new BasketProducts
                            {
                                Id = reader1.GetInt64(0),
                                BasketId = reader1.GetInt64(1),
                                ProductId = reader1.GetInt64(2),
                                Amount = reader1.GetInt32(3)
                            });
                        }
                    }
                    else
                    {
                        return new ResultWrap<List<Purchase>>(ResultValues.NotFound, null);
                    }
                }

                List<Purchase> purchases = new List<Purchase>();
                foreach (var bp in bpList)
                {
                    command = new SqlCommand($"INSERT INTO Purchases(CustomerId,ProductId,Amount) VALUES({basket.CustomerId},{bp.ProductId},{bp.Amount})", _connection);
                    command.ExecuteNonQuery();

                    sql = $@"SELECT TOP (1) * FROM Purchases WHERE CustomerId = {basket.CustomerId} AND ProductId = {bp.ProductId} AND Amount = {bp.Amount} ORDER BY Id DESC";
                    using (var reader2 = new SqlCommand(sql, _connection).ExecuteReader())
                    {
                        if (reader2?.HasRows ?? false)
                        {
                            while (reader2.Read())
                            {
                                purchases.Add(new Purchase()
                                {
                                    Id = reader2.GetInt64(0),
                                    CustomerId = reader2.GetInt64(1),
                                    ProductId = reader2.GetInt64(2),
                                    Amount = reader2.GetInt32(3)
                                });
                            }
                        }
                    }

                    command = new SqlCommand($"DELETE FROM BasketProducts WHERE Id = {bp.Id}", _connection);
                    command.ExecuteNonQuery();
                }

                return new ResultWrap<List<Purchase>>(ResultValues.Success, purchases);
            }
            catch (Exception ex)
            {
                return new ResultWrap<List<Purchase>>(ResultValues.Error, null);
            }
            finally
            {
                _connection.Close();
            }
        }

        private Basket GetBasket(long idCustomer)
        {
            SqlDataReader reader = null;
            try
            {
                _connection.Open();
                string sql = @$"SELECT TOP (1) * FROM Baskets WHERE CustomerId = {idCustomer}";
                var command = new SqlCommand(sql, _connection);
                reader = command.ExecuteReader();

                Basket basket = null;
                if (reader?.HasRows ?? false)
                {
                    while (reader.Read())
                    {
                        basket = new Basket
                        {
                            Id = reader.GetInt64(0),
                            CustomerId = reader.GetInt64(1)
                        };
                    }
                }
                else
                {
                    return null;
                }

                return basket;
            }
            catch (Exception)
            {
                return null;
            }
            finally
            {
                reader?.Close();
                _connection.Close();
            }
        }

    }
}
