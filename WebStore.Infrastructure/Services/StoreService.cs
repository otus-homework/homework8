﻿using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.ExceptionServices;
using System.Threading.Tasks;
using WebStore.Domain.Model;

namespace WebStore.Infrastructure.Services
{
    public class StoreService : IStoreService
    {
        private readonly StoreDbContext _context;

        public StoreService(StoreDbContext context)
        {
            _context = context;
        }

        /// <summary>
        /// Добавляем покупателя
        /// </summary>
        /// <param name="firstName"></param>
        /// <param name="lastName"></param>
        /// <param name="phoneNumber"></param>
        /// <returns></returns>
        public ResultWrap<Customer> CreateCustomer(string firstName, string lastName, string phoneNumber)
        {
            var customer = new Customer(firstName, lastName, phoneNumber);
            _context.Customers.Add(customer);
            _context.SaveChanges();

            return new ResultWrap<Customer>(ResultValues.Success, customer);
        }

        /// <summary>
        /// Получить покупателя
        /// </summary>
        /// <param name="idCustomer"></param>
        /// <returns></returns>
        public async Task<Customer> GetCustomer(long idCustomer)
        {
            var customers = _context.Customers
                .Include(cust => cust.Basket)
                .ThenInclude(bask => bask.BasketProducts);

            return await customers.FirstOrDefaultAsync(x => x.Id == idCustomer);
        }

        /// <summary>
        /// Получить  всех покупателей
        /// </summary>
        /// <returns></returns>
        public ResultWrap<List<Customer>> GetAllCustomers()
        {
            return new ResultWrap<List<Customer>>(ResultValues.Success, _context.Customers.ToList());
        }

        /// <summary>
        /// Создать продукт
        /// </summary>
        /// <param name="name"></param>
        /// <param name="description"></param>
        /// <param name="price"></param>
        /// <returns></returns>
        public ResultWrap<Product> CreateProduct(string name, string description, decimal price)
        {
            var product = new Product(name, description, price);
            _context.Products.Add(product);
            _context.SaveChanges();

            return new ResultWrap<Product>(ResultValues.Success, product);
        }

        /// <summary>
        /// Получить всех продуктов
        /// </summary>
        /// <returns></returns>
        public ResultWrap<List<Product>> GetAllProducts()
        {
            return new ResultWrap<List<Product>>(ResultValues.Success, _context.Products.ToList());
        }
        /// <summary>
        /// Получить продукт
        /// </summary>
        /// <param name="idProduct"></param>
        /// <returns></returns>
        public async Task<Product> GetProduct(long idProduct)
        {
            return await _context.Products.FirstOrDefaultAsync(x => x.Id == idProduct);
        }

        /// <summary>
        /// Посмотреть корзину клиента
        /// </summary>
        /// <param name="idCustomer"></param>
        /// <returns></returns>
        public ResultWrap<Basket> ShowBasket(long idCustomer)
        {
            var customer = GetCustomer(idCustomer).Result;
            if (customer == null)
            {
                return new ResultWrap<Basket>(ResultValues.NotFound, null);
            }

            var basket = GetBasket(customer);
            return new ResultWrap<Basket>(ResultValues.Success, basket);
        }

        /// <summary>
        /// Добавить товар в корзину
        /// </summary>
        /// <param name="idCustomer">Id клиента</param>
        /// <param name="idProduct">Id товара</param>
        /// <param name="amount"></param>
        /// <returns></returns>
        public async Task<ResultWrap<BasketProducts>> AddProductInBasket(long idCustomer, long idProduct, int amount)
        {
            var customer = GetCustomer(idCustomer).Result;
            if (customer == null)
            {
                return new ResultWrap<BasketProducts>(ResultValues.NotFound, null);
            }

            var product = GetProduct(idProduct).Result;
            if (product == null)
            {
                return new ResultWrap<BasketProducts>(ResultValues.NotFound, null);
            }

            var basket = GetBasket(customer);

            BasketProducts bp = null;
            bp = basket.BasketProducts?.FirstOrDefault(x => x.ProductId == product.Id);
            if (bp == null)
            {
                bp = new BasketProducts
                {
                    Basket = basket,
                    Product = product,
                    Amount = amount
                };
                _context.BasketProducts.Add(bp);
            }
            else
            {
                bp.Amount += amount;
            }
            await _context.SaveChangesAsync();

            return new ResultWrap<BasketProducts>(ResultValues.Success, bp); ;
        }

        /// <summary>
        /// Удалить товар из корзины
        /// </summary>
        /// <param name="idCustomer"></param>
        /// <param name="idProduct"></param>
        /// <returns></returns>
        public ResultWrap<BasketProducts> RemoveProductInBasket(long idCustomer, long idProduct)
        {
            var customer = GetCustomer(idCustomer).Result;
            if (customer == null)
            {
                return new ResultWrap<BasketProducts>(ResultValues.NotFound, null); ;
            }

            var product = GetProduct(idProduct);
            if (product == null)
            {
                return new ResultWrap<BasketProducts>(ResultValues.NotFound, null); ;
            }

            var basket = GetBasket(customer);

            BasketProducts bp = null;
            bp = basket.BasketProducts.FirstOrDefault(x => x.ProductId == product.Id);
            if (bp == null)
            {
                return new ResultWrap<BasketProducts>(ResultValues.Error, null); ;
            }
            _context.BasketProducts.Remove(bp);
            _context.SaveChanges();

            return new ResultWrap<BasketProducts>(ResultValues.Success, bp);
        }

        private Basket GetBasket(Customer customer)
        {
            var basket = customer?.Basket;
            if (basket == null)
            {
                basket = new Basket()
                {
                    Customer = customer
                };
                _context.Baskets.Add(basket);
                _context.SaveChanges();
            }

            return basket;
        }

        /// <summary>
        /// Показать покупки покупателя
        /// </summary>
        /// <param name="idCustomer"></param>
        /// <returns></returns>
        public ResultWrap<List<Purchase>> ShowCustomerPurhases(long idCustomer)
        {
            var customer = GetCustomer(idCustomer).Result;
            if (customer == null)
            {
                return new ResultWrap<List<Purchase>>(ResultValues.NotFound, null);
            }

            return new ResultWrap<List<Purchase>>(ResultValues.Success, customer.Purchases?.ToList());
        }

        /// <summary>
        /// Сделать покупку
        /// </summary>
        /// <param name="idCustomer"></param>
        /// <returns></returns>
        public ResultWrap<List<Purchase>> BuyProductInBasket(long idCustomer)
        {
            var customer = GetCustomer(idCustomer).Result;
            if (customer == null)
            {
                return new ResultWrap<List<Purchase>>(ResultValues.NotFound, null);
            }

            var basket = GetBasket(customer);

            List<BasketProducts> bpList = basket.BasketProducts?.ToList();
            if (bpList == null)
            {
                return new ResultWrap<List<Purchase>>(ResultValues.Error, null); ;
            }

            List<Purchase> purchases = new List<Purchase>();
            foreach (var bp in bpList)
            {
                var purchase = Purchase.CreatePurchase(bp);
                _context.Purchases.Add(purchase);
                purchases.Add(purchase);
                _context.BasketProducts.Remove(bp);
            }
            _context.SaveChanges();

            return new ResultWrap<List<Purchase>>(ResultValues.Success, purchases);


        }

    }
}
