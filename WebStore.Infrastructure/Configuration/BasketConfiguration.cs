﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using WebStore.Domain.Model;

namespace WebStore.Infrastructure.Configuration
{
    class BasketConfiguration : IEntityTypeConfiguration<Basket>
    {
        public void Configure(EntityTypeBuilder<Basket> builder)
        {
            builder
                .HasOne(a => a.Customer)
                .WithOne(x => x.Basket)
                .HasForeignKey<Basket>(x => x.CustomerId)
                .IsRequired();

            builder
                .HasMany(a => a.BasketProducts)
                .WithOne(b => b.Basket)
                .HasForeignKey(k => k.BasketId);
        }
    }
}
