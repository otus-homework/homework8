﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;
using WebStore.Domain.Model;

namespace WebStore.Infrastructure.Configuration
{
    class BasketProductsConfiguration : IEntityTypeConfiguration<BasketProducts>
    {
        public void Configure(EntityTypeBuilder<BasketProducts> builder)
        {
            builder.Ignore(p => p.TotalCost);
        }
    }
}
