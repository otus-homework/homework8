﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using WebStore.Domain.Model;

namespace WebStore.Infrastructure.Configuration
{
    class ProductsConfiguration : IEntityTypeConfiguration<Product>
    {
        public void Configure(EntityTypeBuilder<Product> builder)
        {
            builder
                .HasMany(a => a.BasketProducts)
                .WithOne(b => b.Product)
                .HasForeignKey(x => x.ProductId);

            builder
                .HasMany(a => a.Purchases)
                .WithOne(b => b.Product)
                .HasForeignKey(b => b.ProductId);
        }
    }
}
