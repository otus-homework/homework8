﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;
using WebStore.Domain.Model;

namespace WebStore.Infrastructure.Configuration
{
    class CustomerConfiguration : IEntityTypeConfiguration<Customer>
    {
        public void Configure(EntityTypeBuilder<Customer> builder)
        {
            builder.Property(p => p.FirstName)
               .IsRequired()
               .HasMaxLength(100);

            builder.Property(p => p.LastName)
                .IsRequired()
                .HasMaxLength(50);

            builder
                .HasMany(a => a.Purchases)
                .WithOne(b => b.Customer)
                .HasForeignKey(k => k.CustomerId);

            //builder.HasOne(a => a.Basket)
            //    .WithOne(b => b.Customer)
            //    .HasForeignKey<Customer>(x => x.BasketId);
        }
    }
}
