﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.Json.Serialization;

namespace WebStore.Domain.Model
{
    public class Basket
    {
        public long Id { get; set; }
        public long CustomerId { get; set; }
        [JsonIgnore]
        public virtual Customer Customer { get; set; }
        //[JsonIgnore]
        public virtual ICollection<BasketProducts> BasketProducts { get; set; }
    }
}
