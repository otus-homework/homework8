﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.Json.Serialization;

namespace WebStore.Domain.Model
{
    public class BasketProducts
    {
        public long Id { get; set; }
        public long BasketId { get; set; }
        public long ProductId { get; set; }
        [JsonIgnore]
        public virtual Basket Basket { get; set; }
        [JsonIgnore]
        public virtual Product Product { get; set; }
        public int Amount { get; set; }
        public decimal TotalCost
        {
            get { return Amount * Product?.Price ?? 0; }
        }

        //public BasketProducts(Basket basket, Product product, int amount)
        //{
        //    Basket = basket;
        //    Product = product;
        //    Amount = amount;
        //}
    }
}
