﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.Json.Serialization;

namespace WebStore.Domain.Model
{
    public class Purchase
    {
        public long Id { get; set; }
        public long CustomerId { get; set; }
        public long ProductId { get; set; }
        [JsonIgnore]
        public virtual Customer Customer { get; set; }
        [JsonIgnore]
        public virtual Product Product { get; set; }
        public int Amount { get; set; }
        public decimal TotalCost
        {
            get { return Amount * Product?.Price ?? 0; }
        }

        public static Purchase CreatePurchase(BasketProducts bp)
        {
            return new Purchase()
            {
                ProductId = bp.ProductId,
                CustomerId = bp.Basket.CustomerId,
                Amount = bp.Amount
            };
        }
    }
}
