﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WebStore.Domain.Model
{
    public enum ResultValues
    {
        Success,
        NotFound,
        Error
    }
    public class ResultWrap<T>
    {
        public ResultValues ResultValue { get; set; }
        public T ResultObject { get; set; }

        public ResultWrap(ResultValues resultValue, T resultObject)
        {
            ResultValue = resultValue;
            ResultObject = resultObject;
        }        
    }
}
