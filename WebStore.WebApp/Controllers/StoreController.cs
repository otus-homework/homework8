﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Serilog;
using WebStore.Domain.Model;
using WebStore.Infrastructure.Services;

namespace WebStore.WebApp.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class StoreController : ControllerBase
    {
        private readonly IStoreService _service;

        public StoreController(IStoreService storeService)
        {
            _service = storeService;
        }

        [Route("CreateCustomer/{firstName}/{lastName}/{phoneNumber}")]
        [HttpPost]
        public ActionResult CreareCustomer(string firstName, string lastName, string phoneNumber)
        {
            try
            {
                var result = _service.CreateCustomer(firstName,lastName,phoneNumber);
                return GetActionResult(result);
            }
            catch (Exception ex)
            {
                Log.Error(ex, ex.Message);
            }
            return null;
        }

        [Route("GetAllCustomers")]
        [HttpGet]
        public ActionResult GetAllCustomers()
        {
            try
            {
                var result = _service.GetAllCustomers();
                return GetActionResult(result);
            }
            catch (Exception ex)
            {
                Log.Error(ex, ex.Message);
            }
            return null;
        }

        [Route("GetCustomer/{idCustomer}")]
        [HttpGet]
        public ActionResult GetCustomerById(long idCustomer)
        {
            try
            {
                var result = _service.GetCustomer(idCustomer);
                return Ok(result);
            }
            catch (Exception ex)
            {
                Log.Error(ex, ex.Message);
            }
            return null;
        }

        [Route("CreateProduct/{name}/{description}/{price}")]
        [HttpPost]
        public ActionResult CreateProduct(string name, string description, decimal price)
        {
            try
            {
                var result = _service.CreateProduct(name, description, price);
                return GetActionResult(result);
            }
            catch (Exception ex)
            {
                Log.Error(ex, ex.Message);
            }
            return null;
        }

        [Route("GetAllProducts")]
        [HttpGet]
        public ActionResult GetAllProducts()
        {
            try
            {
                var result = _service.GetAllProducts();
                return GetActionResult(result);
            }
            catch (Exception ex)
            {
                Log.Error(ex, ex.Message);
            }
            return null;
        }

        [Route("GetProduct/{idProduct}")]
        [HttpGet]
        public ActionResult GetProductById(long idProduct)
        {
            try
            {
                var result = _service.GetProduct(idProduct);
                return Ok(result);
            }
            catch (Exception ex)
            {
                Log.Error(ex, ex.Message);
            }
            return null;
        }

        [Route("ShowBasket/{idCustomer}")]
        [HttpGet]
        public ActionResult ShowBasket(long idCustomer)
        {
            try
            {
                var result = _service.ShowBasket(idCustomer);
                return GetActionResult(result);
            }
            catch (Exception ex)
            {
                Log.Error(ex, ex.Message);
            }

            return null;
        }        

        [HttpPost("AddProductInBasket/{idCustomer}/{idProduct}/{amount}")]
        public ActionResult<BasketProducts> AddProductInBasket(long idCustomer, long idProduct, int amount)
        {
            try
            {
                var result = _service.AddProductInBasket(idCustomer, idProduct, amount).Result;
                return GetActionResult(result);
            }
            catch (Exception ex)
            {
                Log.Error(ex, ex.Message);
            }

            return null;

        }

        [HttpDelete("RemoveProductInBacket/{idCustomer}/{idProduct}")]
        public ActionResult RemoveProductInBasket(long idCustomer, long idProduct)
        {
            try
            {
                var result = _service.RemoveProductInBasket(idCustomer, idProduct);
                return GetActionResult(result);
            }
            catch (Exception ex)
            {
                Log.Error(ex, ex.Message);
            }

            return null;
        }

        [HttpGet("ShowCustomerPurhases/{idCustomer}")]
        public ActionResult ShowCustomerPurhases(long idCustomer)
        {
            try
            {
                var result = _service.ShowCustomerPurhases(idCustomer);
                return GetActionResult(result);
            }
            catch (Exception ex)
            {
                Log.Error(ex, ex.Message);
            }

            return null;
        }

        [HttpPost("BuyProductInBasket/{idCustomer}")]
        public ActionResult BuyProductInBasket(long idCustomer)
        {
            try
            {
                var result = _service.BuyProductInBasket(idCustomer);
                return GetActionResult(result);
            }
            catch (Exception ex)
            {
                Log.Error(ex, ex.Message);
            }

            return null;
        }

        /// <summary>
        /// Обертка получения результата
        /// </summary>
        private ActionResult GetActionResult<T>(ResultWrap<T> result)
        {
            return result.ResultValue switch
            {
                ResultValues.Success => Ok(result.ResultObject),
                ResultValues.NotFound => NotFound(),
                ResultValues.Error => BadRequest(),
                _ => null,
            };
        }
    }
}